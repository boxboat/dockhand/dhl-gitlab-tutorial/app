build:
	go build -o bin/main cmd/main.go

run:
	go run cmd/main.go

docker:
	docker build --build-arg user=dhl . -t app:latest

clean:
	rm -rf bin