package main

import (
	"embed"
	"html/template"
	"log"
	"net/http"
)

var (
	//go:embed templates/*
	templates embed.FS

	//go:embed static/*
	staticFiles embed.FS

	//go:embed title.txt
	title string
)

func main() {
	staticFS := http.FS(staticFiles)
	fs := http.FileServer(staticFS)

	http.Handle("/static/", fs)

	http.HandleFunc("/", indexHTMLHandler)

	log.Println("Listening on :8080...")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func indexHTMLHandler(w http.ResponseWriter, req *http.Request) {
	indexHTMLTemplate, err := template.ParseFS(templates, "templates/index.html.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	var path = req.URL.Path
	log.Println("Serving request for path", path)
	w.Header().Add("Content-Type", "text/html")

	indexHTMLTemplate.Execute(w, struct {
		Title string
	}{Title: title})

}
