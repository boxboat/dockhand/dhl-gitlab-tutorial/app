####################
# stage: builder image
####################

# TODO add registry
FROM harbor.boxops.boxboat.io/dhl-demo/base/go:commit-master as builder

# create build directory
RUN mkdir /build
WORKDIR /build

# install dependencies
COPY go.mod go.sum /build/
RUN go mod download

# build app
COPY . /build/
RUN make build

####################
# stage: final image
####################

# TODO add registry
FROM harbor.boxops.boxboat.io/dhl-demo/base/debian:commit-master

ARG user

# copy binaries
COPY --from=builder --chown=app:app /build/bin/main /app/

ENV PORT=8080
USER app:app
CMD ["./main"]